using System;
using Gtk;
using System.Net;
using System.Net.Http;

namespace App_meteo_csharp
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Application.Init();



            var app = new Application("org.App_meteo_csharp.App_meteo_csharp", GLib.ApplicationFlags.None);
            app.Register(GLib.Cancellable.Current);

            var win = new Appmeteo();
            app.AddWindow(win);

            win.Show();
            Application.Run();
        }
    }
}
